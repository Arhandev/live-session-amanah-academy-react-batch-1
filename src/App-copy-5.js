import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import DynamicPage from "./components/pages/DynamicPage";
import PageOne from "./components/pages/PageOne";
import PageThree from "./components/pages/PageThree";
import PageTwo from "./components/pages/PageTwo";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageOne />} />
          <Route path="/two" element={<PageTwo />} />
          <Route path="/three" element={<PageThree />} />
          <Route path="/test/:id" element={<DynamicPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
