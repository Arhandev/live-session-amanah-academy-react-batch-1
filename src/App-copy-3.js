import React from "react";

const ThirdComponent = ({ kalimat }) => {
  return (
    <div className="p-6 m-6 bg-red-500">
      <h1 className="text-4xl font-bold">Third Component</h1>
      <p className="text-2xl">{kalimat}</p>
    </div>
  );
};
const SecondComponent = ({ kalimat }) => {
  return (
    <div className="p-6 m-6 bg-blue-500">
      <h1 className="text-4xl font-bold">Second Component</h1>
      <ThirdComponent kalimat={kalimat} />
    </div>
  );
};

const FirstComponent = ({ kalimat }) => {
  return (
    <div className="p-6 m-6 bg-green-500">
      <h1 className="text-4xl font-bold">First Component</h1>
      <SecondComponent kalimat={kalimat} />
    </div>
  );
};

function App() {
  return (
    <div className="p-6 bg-yellow-500">
      <h1 className="text-4xl font-bold">App Component</h1>
      <FirstComponent kalimat={"Halo ini dari App Component"} />
    </div>
  );
}

export default App;
