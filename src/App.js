import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import FormPage from "./components/pages/FormPage";
import HomePage from "./components/pages/HomePage";
import LoginForm from "./components/pages/LoginForm";
import RegisterForm from "./components/pages/RegisterForm";
import TablePage from "./components/pages/TablePage";
import UpdateFormPage from "./components/pages/UpdateFormPage";
import GuestRoute from "./wrapper/GuestRoute";
import ProtectedRoute from "./wrapper/ProtectedRoute";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />

          {/* Protected Route */}
          <Route element={<ProtectedRoute />}>
            <Route path="/table" element={<TablePage />} />
            <Route path="/create" element={<FormPage />} />
            <Route path="/edit/:id" element={<UpdateFormPage />} />
          </Route>

          {/* Guest Route */}
          <Route element={<GuestRoute />}>
            <Route path="/login" element={<LoginForm />} />
            <Route path="/register" element={<RegisterForm />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
