import axios from "axios";
import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const ContextProvider = ({ children }) => {
  const [articles, setArticles] = useState([]);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    console.log("Sedang fetching");
    try {
      setLoading(true);
      const response = await axios.get("https://api-project.amandemy.co.id/api/articles", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(response.data.data);
      console.log("Fetching selesai");
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };
  const fetchProduct = async () => {
    console.log("Sedang fetching");
    try {
      setLoading(true);
      const response = await axios.get("https://api-project.amandemy.co.id/api/products", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(response.data.data);
      console.log("Fetching selesai");
      setProducts(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        articles: articles,
        setArticles: setArticles,
        fetchArticles: fetchArticles,
        loading: loading,
        fetchProduct: fetchProduct,
        setProducts: setProducts,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
