import React, { useEffect, useState } from "react";

function App() {
  const [counter, setCounter] = useState(0);
  const [isMounted, setIsMounted] = useState(false);

  const handleMinus = () => {
    setCounter(counter - 1);
  };

  const handlePlus = () => {
    setCounter(counter + 1);
  };

  useEffect(() => {
    if (isMounted === true) {
      alert("Nilai Counter itu berubah");
    }
  }, [counter]);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  return (
    <div className="m-6">
      <h1 className="text-2xl font-bold text-center">Updating</h1>
      <div className="flex justify-center gap-6 mt-4">
        <button
          onClick={handleMinus}
          className="px-5 py-2 bg-red-600 text-white"
        >
          Minus
        </button>
        <h1 className="text-2xl">{counter}</h1>
        <button
          onClick={handlePlus}
          className="px-5 py-2 bg-green-600 text-white"
        >
          Plus
        </button>
      </div>
    </div>
  );
}

export default App;
