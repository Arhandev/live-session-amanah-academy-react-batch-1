import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

function Navbar() {
  const navigate = useNavigate();
  
  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      //remove localStorage
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/login");
    }
  };
  return (
    <div className="px-16 bg-blue-900 text-white py-4 flex justify-between">
      <div className="flex items-center gap-6 text-lg">
        <Link to="/">
          <p>Home</p>
        </Link>
        <Link to="/table">
          <p>Table</p>
        </Link>
      </div>

      {localStorage.getItem("token") ? (
        <div className="flex gap-4 items-center">
          <div className="text-white text-lg">
            Hai, {localStorage.getItem("username")}
          </div>
          <button
            onClick={onLogout}
            className="bg-red-600 border-2 border-red-400 text-white px-4 py-2 rounded-lg"
          >
            Logout
          </button>
        </div>
      ) : (
        <div className="flex gap-4">
          <Link to="/register">
            <button className="bg-blue-900 border-2 border-blue-200 text-blue-200 px-4 py-2 rounded-lg">
              Register
            </button>
          </Link>
          <Link to="/login">
            <button className="bg-blue-200 text-blue-900  px-4 py-2 rounded-lg">
              Login
            </button>
          </Link>
        </div>
      )}
    </div>
  );
}

export default Navbar;
