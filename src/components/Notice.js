import React, { useEffect, useState } from "react";

function Notice() {
  useEffect(() => {
    return () => {
      alert("Komponent tertutup dari lifecycle");
    };
  }, []);

  const [isDisplay, setIsDisplay] = useState(false);

  const handleChangeDisplay = () => {
    setIsDisplay(!isDisplay);
  };

  return (
    <div>
      <h1 className="py-6 bg-red-700 text-white">Ini dari Notice Component</h1>
      <button
        onClick={handleChangeDisplay}
        className="px-4 py-2 bg-green-600 text-white"
      >
        Klik aku
      </button>
      {isDisplay === true && <h1> Dari Component Notice</h1>}
    </div>
  );
}

export default Notice;
