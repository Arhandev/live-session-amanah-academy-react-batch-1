import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

function UpdateForm() {
  const { fetchArticles } = useContext(GlobalContext);

  const navigate = useNavigate();

  const { id } = useParams();

  const [input, setInput] = useState({
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  });

  const handleChange = (event) => {
    if (event.target.name === "name") {
      setInput({ ...input, name: event.target.value });
    } else if (event.target.name === "content") {
      setInput({ ...input, content: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...input, image_url: event.target.value });
    } else if (event.target.name === "highlight") {
      setInput({ ...input, highlight: event.target.checked }); // checkbox
    }
  };

  const fetchDetail = async () => {
    console.log("Sedang fetching");
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      console.log(response.data.data);
      const article = response.data.data;
      setInput({
        name: article.name,
        content: article.content,
        image_url: article.image_url,
        highlight: article.highlight,
      });
    } catch (error) {
      console.log(error);
    } finally {
    }
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/articles/${id}`,
        {
          name: input.name,
          image_url: input.image_url,
          content: input.content,
          highlight: input.highlight,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      console.log(response);
      fetchArticles();
      alert("Berhasil Mengupdate Article");
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  useEffect(() => {
    fetchDetail();
  }, []);

  return (
    <div className="my-6 border-2 border-black rounded-xl p-8 max-w-2xl mx-auto ">
      <h1 className="text-2xl font-bold text-center mb-6">
        Form Update Articles
      </h1>

      <div className="grid grid-cols-6 gap-y-6">
        <label htmlFor="" className="col-span-2">
          Name
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          type="text"
          placeholder="Masukkan Nama"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          name="name"
          value={input.name}
        />
        <label htmlFor="" className="col-span-2">
          Konten
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          name="content"
          value={input.content}
          type="text"
          placeholder="Masukkan Konten"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <label htmlFor="" className="col-span-2">
          Image URL
        </label>
        <p>:</p>
        <input
          onChange={handleChange}
          type="text"
          value={input.image_url}
          name="image_url"
          placeholder="Masukkan Image URL"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <input
          type="checkbox"
          checked={input.highlight}
          onChange={handleChange}
          name="highlight"
        />
        <label htmlFor="" className="col-span-3">
          Is Highlight?
        </label>
      </div>
      <div className="flex items-center justify-center">
        <button
          onClick={handleUpdate}
          className="bg-green-300 border border-green-600 px-6 py-2 mt-5"
        >
          Update
        </button>
      </div>
    </div>
  );
}

export default UpdateForm;
