import React from "react";
import Navbar from "../Navbar";
import UpdateForm from "../UpdateForm";

function UpdateFormPage() {
  return (
    <div>
      <Navbar />
      <UpdateForm />
    </div>
  );
}

export default UpdateFormPage;
