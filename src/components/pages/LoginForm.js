import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../Navbar";

function LoginForm() {
  const navigate = useNavigate();

  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  const handleChange = (event) => {
    if (event.target.name === "email") {
      setInput({ ...input, email: event.target.value });
    } else if (event.target.name === "password") {
      setInput({ ...input, password: event.target.value });
    }
  };

  const handleSubmit = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: input.email,
          password: input.password,
        }
      );
      localStorage.setItem("username", response.data.data.user.username);
      localStorage.setItem("token", response.data.data.token);
      alert("Berhasil Login!");
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  return (
    <div>
      <Navbar />
      <div className="mt-8 border-2 rounded-lg border-gray-600 p-6 max-w-xl mx-auto">
        <h1 className="text-center text-2xl font-bold mb-7">Login Form</h1>
        <div className="grid grid-cols-12 gap-y-3">
          <label className="col-span-4" htmlFor="">
            Email
          </label>
          <p>:</p>
          <input
            onChange={handleChange}
            type="text"
            name="email"
            className="col-span-7 border-2 px-2 py-1"
            placeholder="Masukkan Email Pengguna"
          />
          <label className="col-span-4" htmlFor="">
            Password
          </label>
          <p>:</p>
          <input
            onChange={handleChange}
            type="password"
            name="password"
            className="col-span-7 border-2 px-2 py-1"
            placeholder="Masukkan Password"
          />
          <button
            onClick={handleSubmit}
            className="col-span-12 bg-blue-600 text-center py-1 text-white rounded-lg mt-5"
          >
            Login
          </button>
        </div>
      </div>
    </div>
  );
}

export default LoginForm;
