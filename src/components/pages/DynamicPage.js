import React from "react";
import { useParams } from "react-router-dom";
import Navbar from "../Navbar";

function DynamicPage() {
  const { id } = useParams();
  return (
    <div>
      <Navbar />
      <div>
        <h1 className="text-center text-4xl font-bold my-10">
          Ini adalah Dynamic Page
        </h1>
        <h1 className="text-center text-4xl font-bold my-10">Value ID: {id}</h1>
      </div>
    </div>
  );
}

export default DynamicPage;
