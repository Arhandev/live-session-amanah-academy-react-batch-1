import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalContext";
import Navbar from "../Navbar";
import Table from "../Table";

function TablePage() {

  

  return (
    <div>
      <Navbar />
      <h1 className="text-2xl font-bold text-center mt-6 mb-4">
        Table Articles
      </h1>
      <div className="flex justify-end max-w-4xl mx-auto">
        <Link to={"/create"}>
          <button className="px-6 py-2 bg-blue-900 text-white rounded-lg">
            Create Articles
          </button>
        </Link>
      </div>
      <Table />
    </div>
  );
}

export default TablePage;
