import React from "react";
import Form from "../Form";
import Navbar from "../Navbar";

function FormPage() {
  return (
    <div>
      <Navbar />
      <Form />
    </div>
  );
}

export default FormPage;
