import React from "react";
import Navbar from "../Navbar";

function PageThree() {
  return (
    <div>
      <Navbar />
      <div>
        <h1 className="text-center text-4xl font-bold my-10">
          Ini adalah Page 3
        </h1>
      </div>
    </div>
  );
}

export default PageThree;
