import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import Article from "../Article";
import Navbar from "../Navbar";

function HomePage() {
  const { articles, setArticles, fetchArticles, loading } =
    useContext(GlobalContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <h1 className="text-2xl font-bold text-center mt-6 mb-4">Articles</h1>
      {loading === true ? (
        <div className="flex items-center justify-center">
          <h1 className="animate-spin w-24 h-24 bg-gradient-to-r from-red-400 to-blue-900 rounded-full"></h1>
        </div>
      ) : (
        <div className="flex flex-col justify-center gap-6 mt-4 max-w-2xl mx-auto">
          {articles.map((item, index) => {
            return <Article article={item} />;
          })}
        </div>
      )}
    </div>
  );
}

export default HomePage;
