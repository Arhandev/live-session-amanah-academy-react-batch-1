import React from "react";
import Navbar from "../Navbar";

function PageTwo() {
  return (
    <div>
      <Navbar />
      <div>
        <h1 className="text-center text-4xl font-bold my-10">
          Ini adalah Page 2
        </h1>
      </div>
    </div>
  );
}

export default PageTwo;
