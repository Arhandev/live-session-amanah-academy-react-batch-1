import React from "react";

function Card({ product }) {
  return (
    <div className="m-4 bg-gray-800 text-white w-40 h-40 text-center">
      <h1 className="text-3xl">{product.name}</h1>
      <h1 className="text-3xl">{product.price}</h1>
    </div>
  );
}

export default Card;
