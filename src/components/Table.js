import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

function Table() {
  const { articles, fetchArticles, loading } = useContext(GlobalContext);
  const [filter, setFilter] = useState({
    highlight: "",
    search: "",
  });

  const handleChange = (event) => {
    if (event.target.name === "highlight") {
      setFilter({ ...filter, highlight: event.target.value });
    } else if (event.target.name === "search") {
      setFilter({ ...filter, search: event.target.value });
    }
  };

  const [filterArr, setFilterArr] = useState([]);

  const onDelete = async (id) => {
    try {
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      fetchArticles();
      alert("Delete article berhasil");
    } catch (error) {
      console.log(error);
      alert("Delete article gagal");
    }
  };

  const handleSearch = () => {
    let articlesArr = structuredClone(articles);

    if (filter.highlight !== "") {
      // filter highlight
      articlesArr = articlesArr.filter((item) => {
        return item.highlight.toString() === filter.highlight;
      });
    }

    if (filter.search !== "") {
      // filter search
      articlesArr = articlesArr.filter((item) => {
        return item.name.toLowerCase().includes(filter.search.toLowerCase());
      });
    }
    setFilterArr(articlesArr);
  };

  const onReset = () => {
    setFilter({
      highlight: "",
      search: "",
    });
    setFilterArr(articles);
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  useEffect(() => {
    setFilterArr(articles);
  }, [articles]);

  return (
    <div className="max-w-4xl mx-auto w-full my-4">
      <div className="flex items-center justify-end gap-4 mb-3">
        <select
          id=""
          className="py-2 px-2 rounded-md bg-white border border-gray-400 w-48"
          name="highlight"
          onChange={handleChange}
          value={filter.highlight}
        >
          <option value="" disabled>
            Filter highlight
          </option>
          <option value="true">Aktif</option>
          <option value="false">Tidak Aktif</option>
        </select>
        <input
          type="text"
          name="search"
          onChange={handleChange}
          value={filter.search}
          className="col-span-8 border-2 rounded-lg px-2 py-1"
          placeholder="Search"
        />
        <button
          onClick={handleSearch}
          className="text-white border-2 border-blue-900 bg-blue-900 rounded-lg px-6 py-2"
        >
          Search
        </button>
        <button
          onClick={onReset}
          className="text-white border-2 border-red-600 bg-red-600 rounded-lg px-6 py-2"
        >
          Reset
        </button>
      </div>
      {loading === true ? (
        <h1 className="text-center text-2xl font-bold">Loading ...</h1>
      ) : (
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Name</th>
              <th className="border border-gray-500 p-2">Content</th>
              <th className="border border-gray-500 p-2">Image</th>
              <th className="border border-gray-500 p-2">Highlight</th>
              <th className="border border-gray-500 p-2">Dibuat oleh</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {filterArr.map((article) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{article.id}</td>
                  <td className="border border-gray-500 p-2">{article.name}</td>
                  <td className="border border-gray-500 p-2">
                    {article.content}
                  </td>
                  <td className="border border-gray-500 p-2">
                    <img src={article.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 p-2">
                    {article.highlight === true ? "Aktif" : "Tidak aktif"}
                  </td>
                  <td className="border border-gray-500 p-2">
                    {article.user !== null ? article.user.name : "-"}
                  </td>
                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <Link to={`/edit/${article.id}`}>
                        <button className="px-3 py-1 bg-yellow-600 text-white">
                          Update
                        </button>
                      </Link>
                      <button
                        onClick={() => onDelete(article.id)}
                        className="px-3 py-1 bg-red-600 text-white"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default Table;
