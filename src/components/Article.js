import React from "react";

function Article({ article }) {
  return (
    <div className="flex gap-4 border-2 border-black rounded-md p-4">
      <div>
        <img
          src={article.image_url}
          alt=""
          className="w-64 h-64 object-cover"
        />
      </div>
      <div className="">
        <h1
          className={`text-2xl font-bold ${
            article.highlight === true
              ? "text-red-500 bg-yellow-100"
              : "text-black"
          }`}
        >
          {article.name}
        </h1>
        <p className="text-lg mt-4">{article.content}</p>
      </div>
    </div>
  );
}

export default Article;
